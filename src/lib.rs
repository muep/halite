#[macro_use] extern crate text_io;

extern crate rand;

pub mod calc;
pub mod networking;
pub mod types;
