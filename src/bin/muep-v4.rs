/* This is to avoid the warning about crate name */
#![allow(non_snake_case)]

extern crate hlt;
extern crate rand;

/*
 * Notice: due to Rust's extreme dislike of (even private!) global
 * mutables, we do not reset the production values of each tile during
 * get_frame. If you change them, you may not be able to recover the
 * actual production values of the map, so we recommend not editing
 * them. However, if your code calls for it, you're welcome to edit
 * the production values of the sites of the map - just do so at your
 * own risk.
*/

use std::collections::HashMap;

use rand::Rng;
use rand::ThreadRng;

use hlt::networking::get_init;
use hlt::networking::get_frame;
use hlt::networking::send_init;
use hlt::networking::send_frame;
use hlt::types::STILL;
use hlt::types::GameMap;
use hlt::types::Location;

const CARDINALS: [Move; 4] = [Move::North, Move::East, Move::South, Move::West];

struct Bot {
    id: u8,
    map: GameMap,
    rng: ThreadRng,
    step_id: u16,
}

impl Bot {
    fn new() -> Bot {
        let (my_id, game_map) = get_init();
        send_init(format!("{}{}", "muep-v4-bot".to_string(), my_id.to_string()));

        let rng = rand::thread_rng();

        Bot {
            id: my_id,
            map: game_map,
            rng: rng,
            step_id: 0,
        }
    }

    fn conquerable(&mut self, l: Location) -> Option<Move> {
        let here = self.map.get_site(l, STILL).clone();

        for c in self.shuffled_cardinals().iter() {
            let there = self.map.get_site(l, c.as_u8()).clone();

            if there.owner == self.id {
                continue;
            }

            if here.strength < there.strength {
                continue;
            }

            return Some(*c);
        }

        None
    }

    fn mv(&mut self, l: Location) -> Move {
        let here = self.map.get_site(l, STILL).clone();

        if here.strength > 200 {
            if (self.step_id / 32) % 2 == 0 {
                return Move::North;
            } else {
                return Move::East;
            }
        }

        match self.conquerable(l) {
            Some(m) => {
                return m;
            },
            None => {},
        }

        match self.reinforcable(l) {
            Some(m) => {
                return m;
            },
            None => {},
        }

        Move::Still
    }

    fn reinforcable(&mut self, l: Location) -> Option<Move> {
        let here = self.map.get_site(l, STILL).clone();

        if here.strength < 15 {
            return None;
        }

        for c in self.shuffled_cardinals().iter() {
            let there = self.map.get_site(l, c.as_u8()).clone();

            if there.owner != self.id {
                continue;
            }

            if there.strength >= here.strength - 15 {
                continue;
            }

            return Some(*c);
        }

        None
    }

    fn shuffled_cardinals(&mut self) -> [Move; 4] {
        let mut cards = CARDINALS;
        self.rng.shuffle(&mut cards);
        cards
    }

    fn step(&mut self) {
        get_frame(&mut self.map);

        let mut moves = HashMap::new();

        for a in 0..self.map.height {
            for b in 0..self.map.width {
                let l = Location { x: b, y: a };

                if self.map.get_site(l, STILL).owner == self.id {
                    moves.insert(l, self.mv(l).as_u8());
                }
            }
        }

        send_frame(moves);

        self.step_id += 1;
    }
}

#[derive(Clone,Copy)]
enum Move {
    East,
    North,
    South,
    Still,
    West,
}

impl Move {
    fn as_u8(self) -> u8 {
        match self {
            Move::East => hlt::types::EAST,
            Move::North => hlt::types::NORTH,
            Move::South => hlt::types::SOUTH,
            Move::Still => hlt::types::STILL,
            Move::West => hlt::types::WEST,
        }
    }
}

fn main() {
    let mut bot = Bot::new();

    loop {
        bot.step();
    }
}
