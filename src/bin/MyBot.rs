/* This is to avoid the warning about crate name */
#![allow(non_snake_case)]

extern crate hlt;
extern crate rand;

/*
 * Notice: due to Rust's extreme dislike of (even private!) global
 * mutables, we do not reset the production values of each tile during
 * get_frame. If you change them, you may not be able to recover the
 * actual production values of the map, so we recommend not editing
 * them. However, if your code calls for it, you're welcome to edit
 * the production values of the sites of the map - just do so at your
 * own risk.
*/

use std::collections::HashMap;

use hlt::calc::BotCommon;
use hlt::calc::ConnectedArea;
use hlt::calc::Move;
use hlt::calc::connected_areas;
use hlt::calc::neighbors;
use hlt::networking::get_init;
use hlt::networking::get_frame;
use hlt::networking::send_init;
use hlt::networking::send_frame;
use hlt::types::STILL;
use hlt::types::Location;

struct Bot {
    common: BotCommon,
    step_id: u16,
}

impl Bot {
    fn new() -> Bot {
        let (my_id, game_map) = get_init();
        send_init(format!("{}{}", "RustBot".to_string(), my_id.to_string()));

        let common = BotCommon::new(my_id, game_map);

        Bot {
            common: common,
            step_id: 0,
        }
    }

    fn mv(&mut self, l: Location, areas: &Vec<ConnectedArea>) -> Move {
        let here = self.common.map.get_site(l, STILL).clone();

        match self.common.conquerable_v2(l) {
            Some(m) => {
                return m;
            },
            None => {},
        }

        if here.strength > 100 {
            /* Move strong pieces toward the border */
            let myarea = match areas.iter().find(|a| a.inside.contains(&l)) {
                Some(a) => a,
                None => {
                    return Move::Still;
                },
            };

            if neighbors(&mut self.common.map, l).iter().any(|n| {
                myarea.border.contains(n)
            }) {
                return Move::Still;
            }

            let closest_border = {
                let mut closest: Location = myarea.border.iter().next().unwrap().clone();
                let mut dist: u16 = self.common.map.get_distance(l, closest);

                for loc in myarea.border.iter() {
                    let loc_dist = self.common.map.get_distance(l, *loc);

                    if loc_dist < dist {
                        closest = *loc;
                        dist = loc_dist;
                    }
                }

                closest
            };

            let closest_angle = self.common.map.get_angle(l, closest_border);
            let pi = std::f64::consts::PI;

            if 1.0 * pi / 4.0 < closest_angle && closest_angle < 3.0 * pi / 4.0 {
                return Move::South;
            } else if -3.0 * pi / 4.0 < closest_angle && closest_angle < -pi / 4.0 {
                return Move::North;
            } else if -pi / 4.0 <= closest_angle && closest_angle <= pi / 4.0 {
                return Move::East;
            } else {
                return Move::West;
            }
        }

        if here.strength > 6 * here.production {
            match self.common.reinforcable(l) {
                Some(m) => {
                    return m;
                },
                None => {},
            }
        }

        Move::Still
    }

    fn step(&mut self) {
        get_frame(&mut self.common.map);

        let areas = connected_areas(&mut self.common.map);

        let mut moves = HashMap::new();

        for a in 0..self.common.map.height {
            for b in 0..self.common.map.width {
                let l = Location { x: b, y: a };
                let owner = self.common.map.get_site(l, STILL).owner;
                if owner == self.common.id {
                    moves.insert(l, self.mv(l, &areas).as_u8());
                }
            }
        }

        send_frame(moves);

        self.step_id += 1;
    }
}

fn main() {
    let mut bot = Bot::new();

    loop {
        bot.step();
    }
}
