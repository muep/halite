use ::std::collections::HashSet;

use ::types::GameMap;
use ::types::Location;

use ::types::STILL;

use ::rand::Rng;

const CARDINALS: [Move; 4] = [Move::North, Move::East, Move::South, Move::West];


pub struct BotCommon {
    pub id: u8,
    pub map: GameMap,
    pub rng: ::rand::ThreadRng,
}

impl BotCommon {
    pub fn new(id: u8, map: GameMap) -> BotCommon {
        BotCommon {
            id: id,
            map: map,
            rng: ::rand::thread_rng(),
        }
    }

    pub fn conquerable(&mut self, l: Location) -> Option<Move> {
        let here = self.map.get_site(l, STILL).clone();

        for c in self.shuffled_cardinals().iter() {
            let there = self.map.get_site(l, c.as_u8()).clone();

            if there.owner == self.id {
                continue;
            }

            if here.strength < there.strength {
                continue;
            }

            return Some(*c);
        }

        None
    }

    pub fn conquerable_v2(&mut self, l: Location) -> Option<Move> {
        let here = self.map.get_site(l, STILL).clone();

        let mut result = None;
        let mut score = 0u32;

        for c in self.shuffled_cardinals().iter() {
            let there = self.map.get_site(l, c.as_u8()).clone();

            if there.owner == self.id {
                continue;
            }

            let wait_time = if here.strength >= there.strength {
                0
            } else {
                let missing = there.strength - here.strength;

                match here.production {
                    0 => 0xff,
                    _ => missing / here.production + 1,
                }
            };

            let efficiency_score =
                255u32 * (there.production as u32) / (1 + there.strength as u32);

            let (wait_score, dir) = match wait_time {
                0 => (2, *c),
                1 => (1, Move::Still),
                _ => (0, Move::Still),
            };

            let c_score = efficiency_score * wait_score;

            if c_score > score {
                score = c_score;
                result = Some(dir);
            }
        }

        result
    }

    pub fn reinforcable(&mut self, l: Location) -> Option<Move> {
        let here = self.map.get_site(l, STILL).clone();

        if here.strength < 15 {
            return None;
        }

        for c in self.shuffled_cardinals().iter() {
            let there = self.map.get_site(l, c.as_u8()).clone();

            if there.owner != self.id {
                continue;
            }

            if there.strength >= here.strength - 15 {
                continue;
            }

            return Some(*c);
        }

        None
    }

    pub fn shuffled_cardinals(&mut self) -> [Move; 4] {
        let mut cards = CARDINALS;
        self.rng.shuffle(&mut cards);
        cards
    }
}

pub struct ConnectedArea {
    pub border: HashSet<Location>,
    pub inside: HashSet<Location>,
}

#[derive(Clone,Copy)]
pub enum Move {
    East,
    North,
    South,
    Still,
    West,
}

impl Move {
    pub fn as_u8(self) -> u8 {
        match self {
            Move::East => ::types::EAST,
            Move::North => ::types::NORTH,
            Move::South => ::types::SOUTH,
            Move::Still => ::types::STILL,
            Move::West => ::types::WEST,
        }
    }
}

pub fn connecteds(map: &mut GameMap, l: Location) -> ConnectedArea {
    use ::types::STILL;

    let id = map.get_site(l, STILL).owner;

    let mut border = HashSet::<Location>::with_capacity(64);
    let mut inside = HashSet::<Location>::with_capacity(64);

    inside.insert(l);
    for n in neighbors(map, l).iter() {
        border.insert(*n);
    }

    while let Some(lb) = match border.iter().find(|l| map.get_site(**l, STILL).owner == id) {
        Some(n) => Some(n.clone()),
        None => None,
    } {
        /* The item in border should actually be included in the inside set */
        border.remove(&lb);
        inside.insert(lb);

        /* Add its neighbors into the border */
        for ln in neighbors(map, lb).iter() {
            if !inside.contains(ln) {
                border.insert(*ln);
            }
        }
    }

    /*
    while border.iter().map(|lb| {
        map.get_site(lb, STILL).owner;
    }*/

    ConnectedArea {
        border: border,
        inside: inside,
    }
}

pub fn connected_areas(map: &mut GameMap) -> Vec<ConnectedArea> {
    let width = map.width;
    let height = map.height;

    let mut result = Vec::with_capacity(16);
    let mut seen: HashSet<Location> =
        HashSet::with_capacity((width * height) as usize);

    for row in 0..map.height {
        for col in 0..map.width {
            let location = Location { x: col, y: row };

            if seen.contains(&location) {
                continue;
            }

            let area = connecteds(map, location);

            seen.extend(&area.inside);
            result.push(area);

            seen.insert(location);
        }
    }

    result
}

pub fn neighbors(map: &mut GameMap, l: Location) -> [Location; 4] {
    [
        map.get_location(l, ::types::EAST).clone(),
        map.get_location(l, ::types::NORTH).clone(),
        map.get_location(l, ::types::SOUTH).clone(),
        map.get_location(l, ::types::WEST).clone(),
    ]
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use ::types::GameMap;
    use ::types::Location;
    use ::types::Site;

    use super::connecteds;
    use super::connected_areas;
    use super::neighbors;

    const S1: Site = Site {
        owner: 1,
        strength: 10,
        production: 1,
    };

    const S2: Site = Site {
        owner: 2,
        strength: 10,
        production: 1,
    };

    fn example_map() -> GameMap {
        GameMap {
            width: 6,
            height: 6,
            contents: vec![
                vec![S1, S1, S1, S1, S1, S1],
                vec![S1, S1, S1, S1, S2, S1],
                vec![S1, S1, S2, S2, S1, S1],
                vec![S1, S1, S1, S2, S1, S1],
                vec![S1, S1, S1, S1, S1, S1],
                vec![S1, S1, S1, S1, S1, S1],
            ],
        }
    }

    #[test]
    fn test_connecteds() {
        let mut map = example_map();

        let inside_array = [
            Location {x: 2, y: 2},
            Location {x: 3, y: 2},
            Location {x: 3, y: 3},
        ];

        let border_array = [
            Location {x: 1, y: 2},
            Location {x: 2, y: 1},
            Location {x: 2, y: 3},
            Location {x: 3, y: 1},
            Location {x: 3, y: 4},
            Location {x: 4, y: 2},
            Location {x: 4, y: 3},
        ];

        let inside_set: HashSet<Location> =
            inside_array.iter().map(|x| *x).collect();

        let border_set: HashSet<Location> =
            border_array.iter().map(|x| *x).collect();

        let cons = connecteds(&mut map, inside_array[0]);


        assert_eq!(inside_set, cons.inside);
        assert_eq!(border_set, cons.border);
    }

    #[test]
    fn test_connected_areas() {
        let mut map = example_map();

        let areas = connected_areas(&mut map);

        assert_eq!(areas.len(), 3);
    }

    #[test]
    fn test_neighbors() {
        let mut map = example_map();

        let l = Location { x: 2, y: 2 };

        let expected_neighbors = [
            Location { x: 1, y: 2 },
            Location { x: 3, y: 2 },
            Location { x: 2, y: 1 },
            Location { x: 2, y: 3 },
        ];

        let actual_neighbors = neighbors(&mut map, l);

        let en_set: HashSet<Location> =
            expected_neighbors.iter().map(|x| *x).collect();
        let an_set: HashSet<Location> =
            actual_neighbors.iter().map(|x| *x).collect();

        assert_eq!(en_set, an_set);
    }
}
